title: "Data z mobilů:"
perex: "Praha se denně nafoukne o polovinu, v centru jsou návštěvníci v převaze"
authors: ["Jan Cibulka", "Petr Kočí"]
coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
coverimg_note: "Foto <a href='#'>ČTK</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1"]
---

Skoro 140 tisíc lidí přijede do Prahy každý den za prací. Míří hlavně do kancelářských budov a do centra města. S nimi pak dorazí i přes půl milionu návštěvníků. Ukázala to analýza dat o pohybu mobilních telefonů, kterou si nyní město nechává zpracovat.

Během dne se lidé v Praze soustřeďují v okolí administrativních center a obchodů, po poledni se tak nejvíc zahustí okolí Andělu, centrum Prahy (Vodičkova ulice a Petrské náměstí) a Brumlovka nedaleko Budějovické. Na poslední jmenované adrese sídlí řada velkých korporací, mezi nimi i Microsoft nebo ČEZ. Pro srovnání, na zmíněných místech je ve „špičce“ okolo šesti stovek osob na jeden hektar, průměr Prahy je 25 obyvatel na [hektar](https://cs.wikipedia.org/wiki/Hektar) (Václavské náměstí má rozlohu asi 4 hektary).

Naopak v noci se nejvíc lidí se „namačká“ ve Vršovicích, jde o 430 osob na hektar.

Detaily si můžete prohlédnout v následující mapě, kterou z dat mobilních operátorů zpracoval pražský [Institut plánování a rozvoje](http://www.iprpraha.cz/).

_Pokud vás zajímá, kolik lidí bývá ve dne a v noci ve vaší čtvrti, najděte si ji na mapě, přepněte se na časové řezy, vyberte přepínač hustota osob a potom pohybujte posuvníkem nahoře._

<iframe src="https://app.iprpraha.cz/apl/app/dynamika-obyvatelstva/#" width="1000" height="710" scrolling="no" frameborder="0"></iframe>

Kromě zaměstnanců do Prahy směřují také návštěvníci, nejvíc (přes 40 tisíc) do okolí Vodičkovy ulice, dále obecně do centra města a na ruzyňské letiště (necelých 30 tisíc). Uvnitř Prahy lidé cestují zejména z velkých sídlišť na Praze 10 a 11 do Prahy 4 (asi 14 000 osob), z Prahy 6 na Prahu 1 (cca 6000 osob), ale třeba i z Prahy 4 do centra (rovněž přibližně 6000 osob).

_"Významnou část návštěvníků pak tvoří zahraniční i domácí turisté nebo pracovníci na služebních cestách. Místa s nejvyšším výskytem návštěvníků je celkem nepřekvapivě centrum Prahy, následováno Letištěm Václava Havla a dále nemocnice Motol. Významným magnetem jsou také Smíchovské nádraží, areály vysokých škol, tedy Albertov a Dejvice, a další místa. V centru Prahy tvoří návštěvníci ve špičkových hodinách téměř 60% všech přítomných osob,"_ říká k tomu šéf sekce protorových informací na IPR Jiří Čtyroký.

Za zmínku stojí i sídliště a obce okolo Prahy. Například ve Kbelích či Letňanech se kvůli dojíždění před den přesune 71 % všech obyvatel, podobné je to i Čakovic a Černého Mostu. Naopak nejmenší pohyby obyvatel ukazují mobilní telefony v Podolí či na Vypichu, tam se denně vymění přibližně třetina všech lidí. 

Z mimopražských lokalit nejvíce dojíždějí lidé z Hostivic na Prahu 6 (cca 1300 osob), což je pro srovnání podobné jako dojížďky mezi Královým Dvorem a Berounem.

## Data na prodej

I pokud netelefonujeme nebo neposíláme SMS, telefon pravidelně komunikuje s vysílači v síti mobilního operátora. Z dat o síle signálu a použité anténě je možné odvodit, kde se přibližně telefon nachází a jak dlouhou dobu na místě strávil.

Mobilní operátor má [zákonnou povinnost](https://www.zakonyprolidi.cz/cs/2005-127/zneni-20160919#p97-3) takové informace uchovávat, a to půl roku do minulosti, přistupovat k nim ale mohou jen bezpečnostní složky, typicky policie nebo rozvědka.

Operátoři ale obdobné informace mohou anonymizovat (tedy zbavit identifikace konkrétního uživatele) a agregovat, tedy spojit informace o jednotlivcích do jakýchsi skupin. Výsledná obecná čísla pak přeprodávají dál, například marketingovým firmám či dopravním analytikům. Zpětně z nich nejde dovodit, kde se pohybuje každý jednotlivec, dávají ale určitý přehled o obecném chování obyvatel nějakého místa.